//http://localhost:8080/api)
var express = require("express");       
var app = express();
var bodyParser = require("body-parser");
var request = require('request');
var rp = require('request-promise');
const fs = require('fs');
const jsonfile = require('jsonfile');
const file = './data/fbiFull.json';
let tempArr = [];
let categoriesArr = [];
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;
var router = express.Router();

app.use("/api", router);
app.listen(port);
console.log("Host Running on port " + port);
/* On startup, pull fbi data, create new array of objects with normalised items */
//populate_JSON_File();
//determine_JSON_length();
/* CORS handling */
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

calculateSize();
function calculateSize() {
    var options = {
      uri: 'https://api.fbi.gov/wanted/v1/list?',
      json: true // Automatically parses the JSON string in the response
    };

  rp(options)
      .then(async function (obj) {
        let total = 1;
       // let total = obj['total']/20;
        let maxPageCount = parseInt(total) + 1;
        let maxPageCountTemp = parseInt(total);
        let offsetPageNumber = maxPageCountTemp;
        for(i = 1; i < maxPageCount; i++) {
          await sleep(1000);
          obtain(i, offsetPageNumber)
        }
      })
};

function obtain(page, offsetPageNumber) {
  var options = {
    uri: 'https://api.fbi.gov/wanted/v1/list?page=' + page,
    json: true
  };
 
rp(options)
    .then(function (obj) {
          for(var item of obj['items']) {
              Object.keys(item).forEach(function eachKey(key) { 
                if(item[key] == null) {
                  item[key] = "No " + key + " information provided.";
              }
          })
      }
      return obj
    }).then(obj => {
      var page = obj.items;
     return page;
    }).then(page => {
      tempArr.push(page);
    }).then(() => {
      console.log("Processing Page =>" + tempArr.length + " of " + offsetPageNumber)
      if(tempArr.length == offsetPageNumber ) {
        var flattened = [].concat.apply([],tempArr);
        console.log("Completed array populating => Final Murderer Count: " + flattened.length);
        format_data(flattened);
      }
    })
    .catch(function (err) {
      console.log(err);
    });
};

function generateCategories(category) {
  if(categoriesArr.includes(category)) {
  //Category exists
  } else {
    categoriesArr.push(category)
    console.log(category)
  }
}

function format_data(obj) {
      var criminalArray = [];
      for(var item of obj) {
        if(item.subjects.length < 1) {
          categoryOverride = 'Uncategorized'
        } else {
          categoryOverride = item.subjects[0];
        }
        const criminal = {
          id:item.uid,
          name: item.title,
          description: item.details,
          caution: item.caution,
          images: item.images[0],
          url: item.url,
          classification: item.classification,
          category:categoryOverride
        };
        criminalArray.push(criminal);
       // generateCategories(item.subjects[0]);
     }

      jsonfile.writeFile(file, criminalArray)
    .then(() => {
      console.log('Data Operation Completed')
    })
    .catch(error => console.error(error))
}

app.get('/api/fbi/data', (req, res) => {
  jsonfile.readFile(file)
  .then(data => { 
    res.send(data)
  })
  .catch(error => console.error(error))
});

app.get('/api/fbi/search', (req, res) => {
  jsonfile.readFile(file)
  .then(data => { 
    const searchStr = req.query.name;
    var arrayFound = data.filter(x => x.name.toLowerCase().includes(searchStr.toLowerCase()))

    if(arrayFound == '') {
      res.send(arrayFound);
    } else {
      res.send(arrayFound);
    }
  })
  .catch(error => console.error(error))
});

app.get('/api/fbi/byCategory', (req, res) => {
  jsonfile.readFile(file)
  .then(data => { 
    var fullArr = data;
    const searchStr = req.query.category;
    var arrayFound = data.filter(x => x.category.toLowerCase().includes(searchStr.toLowerCase()))

    if(arrayFound == '') {
      res.send(fullArr);
    } else {
      res.send(arrayFound);
    }
  })
  .catch(error => console.error(error))
});


